package com.budzek.cryptolab.akka.scala

import java.lang
import java.util.concurrent.TimeUnit
import java.util.logging.Logger

import akka.NotUsed
import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.model.HttpMethods._
import akka.http.scaladsl.model.{HttpRequest, HttpResponse, Uri}
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{BroadcastHub, Keep, Sink, Source}
import com.budzek.cryptolab.model.scala.BittrexModel.{RootObject, Ticker}
import com.budzek.cryptolab.model.scala.{BinanceModel, NBPModel, PoloniexModel}
import org.reactivestreams.Publisher
import spray.json.DefaultJsonProtocol

import scala.collection.JavaConverters
import scala.collection.concurrent.TrieMap
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.duration.{FiniteDuration, _}

object Operation extends Enumeration {
  val BID = Value
  val ASK = Value
}

trait PublicApi {
}


trait JsonSupport extends SprayJsonSupport with DefaultJsonProtocol {
  implicit val system: ActorSystem
  implicit val materializer: ActorMaterializer
}

trait SingleTickerSupport[TICKER] extends JsonSupport {
  def toTicker(httpResponse: HttpResponse): Future[TICKER]
  def toPrice(httpResponse: HttpResponse): Future[java.lang.Double]
}


trait MultiTickerSupport[TICKER] extends JsonSupport {
  def toTicker(httpResponse: HttpResponse): Future[Map[String, TICKER]]
  def toPrice(ticker: Map[String, TICKER], symbol: String): Future[java.lang.Double]
}

trait BittrexJsonSupport extends SingleTickerSupport[Ticker] {
  implicit val tickerFormat = jsonFormat3(Ticker)
  implicit val rootObjectFormat = jsonFormat3(RootObject[Ticker])

  override def toTicker(httpResponse: HttpResponse): Future[Ticker] = {
    Unmarshal(httpResponse.entity).to[RootObject[Ticker]].map(root => root.result)
  }

  override def toPrice(httpResponse: HttpResponse): Future[java.lang.Double] = {
    toTicker(httpResponse).map(ticker => java.lang.Double.valueOf(ticker.Last))
  }
}

trait BinanceJsonSupport extends SingleTickerSupport[BinanceModel.SymbolTicker] {
  implicit val tickerFormat = jsonFormat2(BinanceModel.SymbolTicker)

  override def toTicker(httpResponse: HttpResponse): Future[BinanceModel.SymbolTicker] = {
    Unmarshal(httpResponse.entity).to[BinanceModel.SymbolTicker]
  }

  override def toPrice(httpResponse: HttpResponse): Future[java.lang.Double] = {
    toTicker(httpResponse).map(ticker => java.lang.Double.parseDouble(ticker.price))
  }
}

trait NBPJsonSupport extends SingleTickerSupport[NBPModel.Ticker] {
  implicit val rateFormat = jsonFormat3(NBPModel.Rate)
  implicit val tickerFormat = jsonFormat4(NBPModel.Ticker)

  override def toTicker(httpResponse: HttpResponse): Future[NBPModel.Ticker] = {
    Unmarshal(httpResponse.entity).to[NBPModel.Ticker]
  }

  override def toPrice(httpResponse: HttpResponse): Future[java.lang.Double] = {
    toTicker(httpResponse).map(ticker => ticker.rates.head.mid) //FIXME this is not null safe
  }
}

trait PoloniexJsonSupport extends MultiTickerSupport[PoloniexModel.SymbolTicker]{
  implicit val tickerSymbolFormat = jsonFormat10(PoloniexModel.SymbolTicker)
//  implicit val tickerFormat = jsonFormat1[PoloniexModel.Ticker]

  override def toTicker(httpResponse: HttpResponse): Future[Map[String, PoloniexModel.SymbolTicker]] = {
    Unmarshal(httpResponse.entity).to[Map[String, PoloniexModel.SymbolTicker]]
  }

  def toPrice(ticker: Map[String, PoloniexModel.SymbolTicker], symbol: String): Future[java.lang.Double] = {
    ticker.get(symbol) match {
      case Some(x) => return Future.successful(java.lang.Double.parseDouble(x.last))
      case None => return Future.failed(new IllegalStateException(s"Pair symbol $symbol is not present in the Poloniex ticker"))
    }
  }
}

trait ExchangeProvider[TICKER] extends PublicApi with JsonSupport with MarketSupport {
  val log = Logger.getLogger(this.getClass.getName)

  val http = Http()
  val pricePublishers = TrieMap[String, Publisher[java.lang.Double]]()
  lazy val identityPublisher = Source.repeat(java.lang.Double.valueOf(1)).throttle(1, 1.second).runWith(Sink.asPublisher(true))

  def getPriceRequestUri(c1: String, c2: String): Uri

  def getPriceRequest(c1: String, c2: String): HttpRequest = {
    HttpRequest(method = GET, uri = getPriceRequestUri(c1, c2))
  }

  def getPricePublisher(marketCurrency: String, currency: String, rqInterval: FiniteDuration): Publisher[java.lang.Double] = {
    //TODO this is a hack to handle situation where marketCurrency and currency are equal
    if (marketCurrency.equals(currency))
      return identityPublisher

    //SHOWME
    synchronized {
      return pricePublishers.getOrElseUpdate(cacheKey(mapName(marketCurrency), mapName(currency)),
        createPricePublisher(mapName(marketCurrency), mapName(currency), rqInterval))
    }
  }

  def cacheKey(marketCurrency: String, currency: String): String = {
    return s"$marketCurrency-$currency"
  }

  def createPricePublisher(marketCurrency: String, currency: String, rqInterval: FiniteDuration): Publisher[java.lang.Double] = {
    log.fine(s"${this.getClass().getSimpleName()} createPricePublisher $marketCurrency-$currency every $rqInterval")

    source(marketCurrency, currency, rqInterval).runWith(Sink.asPublisher(true)) // true = fanout broadcast
  }

  def source(marketCurrency: String, currency: String, rqInterval: FiniteDuration): Source[lang.Double, NotUsed]
}

trait SingleTickerExchangeProvider[TICKER] extends ExchangeProvider[TICKER] with SingleTickerSupport[TICKER] {
  override def source(marketCurrency: String, currency: String, rqInterval: FiniteDuration): Source[lang.Double, NotUsed] = {
    return Source
      .repeat(HttpRequest(uri = getPriceRequestUri(marketCurrency, currency)))
      .throttle(1, rqInterval)
      .mapAsync(1)(Http().singleRequest(_).flatMap(x => toPrice(x))) // though features finish async, the output stream is ordered same as input
  }
}


class BittrexProvider(implicit val system: ActorSystem, implicit val materializer: ActorMaterializer) extends SingleTickerExchangeProvider[Ticker] with BittrexJsonSupport {
  def getUri(action: String, params: String): Uri = {
    println(s"https://bittrex.com/api/v1.1/public/$action?$params")

    Uri(s"https://bittrex.com/api/v1.1/public/$action?$params")
  }

  def getPriceRequestUri(c1: String, c2: String): Uri = {
    getUri("getticker", s"market=$c1-$c2")
  }
}

class BinanceProvider(implicit val system: ActorSystem, implicit val materializer: ActorMaterializer) extends SingleTickerExchangeProvider[BinanceModel.SymbolTicker] with BinanceJsonSupport {
  def getUri(action: String, params: String): Uri = {
    Uri(s"https://api.binance.com/api/v1/$action?$params")
  }

  def getPriceRequestUri(c1: String, c2: String): Uri = {
    getUri("ticker/price", s"symbol=$c2$c1")
  }
}

class NBPProvider(implicit val system: ActorSystem, implicit val materializer: ActorMaterializer) extends SingleTickerExchangeProvider[NBPModel.Ticker] with NBPJsonSupport {
  def getUri(action: String, params: String): Uri = {
    Uri(s"http://api.nbp.pl/api/exchangerates/rates/a/$params/?format=json")
  }

  def getPriceRequestUri(c1: String, c2: String): Uri = {
    if(!c1.equals("PLN"))
      throw new IllegalArgumentException("Only PLN market is supported by NBP")

    getUri("", c2)
  }
}

trait MarketSupport {
  val staticMappings: Map[String, String] = Map()
  def mapName(name: String): String = staticMappings.getOrElse(name, name);
}

trait PoloniexMarketSupport extends MarketSupport {
  override val staticMappings: Map[String, String] = Map(
    "XLM" -> "STR"
  )
}

object Exchange extends Enumeration {
  type Exchange = Value
  val BITTREX, BINANCE, POLONIEX, NBP = Value

  def valuesAsJava(): java.lang.Iterable[Exchange.Value] = {
    return JavaConverters.asJavaCollectionConverter(values).asJavaCollection
  }
}

class PoloniexProvider(implicit val system: ActorSystem, implicit val materializer: ActorMaterializer) extends ExchangeProvider[PoloniexModel.Ticker] with PoloniexJsonSupport with PoloniexMarketSupport {
  val commonTickerSourceMap = collection.mutable.Map[FiniteDuration, Source[Map[String, PoloniexModel.SymbolTicker], NotUsed]]();

  def getPriceRequestUri(c1: String, c2: String): Uri = {
    throw new IllegalStateException("Poloniex is using a single common ticker for all pairs")
  }

  def getUri(action: String, params: Option[String]): Uri = {
    Uri(s"https://poloniex.com/public?command=$action${params.map(p => s"?$p").getOrElse("")}")
  }

  def getPriceRequestUri(): Uri = {
    getUri("returnTicker", Option.empty)
  }

  override def source(marketCurrency: String, currency: String, rqInterval: FiniteDuration): Source[lang.Double, NotUsed] = {
    commonTickerSourceMap
      .getOrElseUpdate(rqInterval, createCommonTickerSource(rqInterval))
      .mapAsync(1)(x => toPrice(x, pairKey(marketCurrency, currency)))
  }

  //SHOWME
  // Using the BroadcastHub
  // https://doc.akka.io/docs/akka/2.5/stream/stream-dynamic.html
  def createCommonTickerSource(rqInterval: FiniteDuration): Source[Map[String, PoloniexModel.SymbolTicker], NotUsed] = {
    return Source
      .repeat(HttpRequest(uri = getPriceRequestUri()))
      .throttle(1, rqInterval)
      .mapAsync(1)(Http().singleRequest(_))
      .mapAsync(1)(x => toTicker(x))// though features finish async, the output stream is ordered same as inputs
      .toMat(BroadcastHub.sink(bufferSize = 16))(Keep.right)
      .run()
  }

  def pairKey(marketCurrency: String, currency: String): String = {
    return s"${marketCurrency}_$currency"
  }
}

object CryptoCalculus {

  import Exchange._

  implicit val system = ActorSystem("Sys")
  implicit val materializer = ActorMaterializer()

  val providers = Map(
    BITTREX -> new BittrexProvider(),
    BINANCE -> new BinanceProvider(),
    POLONIEX -> new PoloniexProvider(),
    NBP -> new NBPProvider()

  )

  def pricePublisher(marketCurrency: String = "BTC", currency: String, exchange: Exchange = BITTREX, rqInterval: java.time.Duration): Publisher[java.lang.Double] = {
    return providers(exchange).getPricePublisher(marketCurrency, currency, FiniteDuration(rqInterval.toNanos, TimeUnit.NANOSECONDS))
  }
}
