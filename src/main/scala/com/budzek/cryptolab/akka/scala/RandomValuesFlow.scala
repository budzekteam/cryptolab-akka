package com.budzek.cryptolab.akka.scala

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Sink, _}
import org.reactivestreams.{Publisher, Subscriber}


class RandomValuesFlow {
  implicit val system = ActorSystem("Sys")
  implicit val materializer = ActorMaterializer()

//  def pipe(publisher: Publisher[Long], subscriber: Subscriber[Long])(flowProducer: () => ): Unit ={
//
//  }

  def start(publisher: Publisher[Long], subscriber: Subscriber[Long]): Unit = {
    val flow = Flow[Long].map(n => 3*n)
    Source.fromPublisher(publisher).via(flow).runWith(Sink.fromSubscriber(subscriber))
  }
}
