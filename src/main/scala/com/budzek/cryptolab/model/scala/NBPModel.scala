package com.budzek.cryptolab.model.scala

object NBPModel {
  case class Rate(
                    no: String,
                    effectiveDate: String,
                    mid: Double
                  )
  case class Ticker(
                             table: String,
                             currency: String,
                             code: String,
                             rates: List[Rate]
                           )

}
