package com.budzek.cryptolab.model.scala

object BinanceModel {
  case class Ticker(tickers: SymbolTicker)

  case class SymbolTicker(symbol: String, price: String)
}
