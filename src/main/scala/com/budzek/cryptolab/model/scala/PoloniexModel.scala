package com.budzek.cryptolab.model.scala

object PoloniexModel {

  case class Ticker(tickers: Map[String, SymbolTicker])


  case class SymbolTicker(
                           id: Int,
                           last: String,
                           lowestAsk: String,
                           highestBid: String,
                           percentChange: String,
                           baseVolume: String,
                           quoteVolume: String,
                           isFrozen: String,
                           high24hr: String,
                           low24hr: String
                         )

}
