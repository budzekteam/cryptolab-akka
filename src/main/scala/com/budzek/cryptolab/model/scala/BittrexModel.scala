package com.budzek.cryptolab.model.scala

object BittrexModel {
  case class RootObject[T] (
    success: Boolean,
    message: String,
    result: T
  )

  case class Market (
    MarketCurrency: String,
    BaseCurrency: String,
    MarketCurrencyLong: String,
    BaseCurrencyLong: String,
    MinTradeSize: Double,
    MarketName: String,
    IsActive: Boolean,
    IsRestricted: Boolean,
    Created: String,
    Notice: String,
    IsSponsored: Boolean,
    LogoUrl: String
  )

  case class Ticker (
    Bid: Double,
    Ask: Double,
    Last: Double
  )
}
