package com.budzek.cryptolab.model;

import com.budzek.cryptolab.akka.scala.Exchange;
import io.reactivex.Flowable;
import lombok.AllArgsConstructor;
import lombok.Data;
import scala.Enumeration;

import java.util.Optional;


@Data
@AllArgsConstructor
public class ExchangeChain {
    Optional<ExchangeChain> previous;
    String fromCurrency;
    String toCurrency;
    Enumeration.Value exchangeProvider;

    public static ExchangeChain start() {
        return new ExchangeChain(Optional.empty(), null, null, null);
    }

    public ExchangeChain via(String fromCurrency, String toCurrency, Enumeration.Value exchange) {
        return new ExchangeChain(Optional.of(this), fromCurrency, toCurrency, exchange);
    }

    public Flowable<Double> exchange(Flowable<Double> rootValuePublisher, PricePublisherProvider pricePublisherProvider) {
        return previous
               .map(prev -> Flowable.combineLatest(
                        prev.exchange(rootValuePublisher, pricePublisherProvider),
                        pricePublisherProvider.get(fromCurrency, toCurrency, exchangeProvider),
                        (price, value) -> price * value))
                .orElse(rootValuePublisher);
//                .doOnNext(d -> System.out.println(String.format("ExchangeChain publishing %s-%s-%s: %s",
//                        Optional.ofNullable(fromCurrency).orElse(""),
//                        Optional.ofNullable(toCurrency).orElse(""),
//                        Optional.ofNullable(exchangeProvider).map(e -> e.toString()).orElse(""),
//                        d.toString())));
    }

    public static ExchangeChain btcToUsdToPln() {
        //SHOWME
        return ExchangeChain.start()
                .via("BTC", "USD", Exchange.BITTREX())
                .via("USD", "PLN", Exchange.NBP());
    }
}
