package com.budzek.cryptolab.model;

import com.budzek.cryptolab.akka.scala.CryptoCalculus;
import io.reactivex.Flowable;
import lombok.extern.java.Log;
import scala.Enumeration;

import java.time.Duration;
import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Log
public class WalletValuator {

    private static Map<String, Flowable<Double>> pricePublishers = new ConcurrentHashMap<>();

    private final Enumeration.Value exchange;

    public WalletValuator(Enumeration.Value exchange) {
        this.exchange = exchange;
    }

    public Flowable<Double> valuePublisher(Wallet wallet, String baseCurrency, Duration rqInterval) {
        log.fine(String.format("WalletValuator.valuePublisher %s every %s", baseCurrency, rqInterval.toString()));

        return Flowable.fromArray(wallet.getItems())
                .map(walletItem -> pricePublisher(baseCurrency, walletItem.getCurrency(), exchange, rqInterval).map(price -> price * walletItem.getAmount()))
                .toList()
                .flatMapPublisher(publishers -> Flowable.combineLatest(publishers, valuesArray -> Arrays.stream(valuesArray).mapToDouble(Double.class::cast).sum()));
    }

    public Flowable<Double> valuePublisher(Wallet wallet, String baseCurrency, Duration rqInterval, ExchangeChain exchangeChain) {
        return exchangeChain.exchange(valuePublisher(wallet, baseCurrency, rqInterval),
                ((fromCurrency, toCurrency, exchangeProvider) -> pricePublisher(toCurrency, fromCurrency, exchangeProvider, rqInterval)));
    }

    private Flowable<Double> pricePublisher(String baseCurrency, String currency, Enumeration.Value exchange, Duration rqInterval) {
        String key = key(baseCurrency, currency, exchange, rqInterval);

        log.fine(String.format("WalletValuator.pricePublisher %s", key));

        //SHOWME //CHECKME
        //NOT TRUE ANTMORE having cached Publishers of http request in CryptoCalculus and not cached Flowable here ensures that on error in this flowable won't impact calculation for other flow

        return pricePublishers.computeIfAbsent(key, k -> Flowable.fromPublisher(CryptoCalculus.pricePublisher(baseCurrency, currency, exchange, rqInterval)));
    }

    private String key(String baseCurrency, String currency, Enumeration.Value exchange, Duration rqInterval) {
        return String.format("%s-%s-%s-%s", baseCurrency, currency, exchange.toString(), rqInterval.toString());
    }

}

