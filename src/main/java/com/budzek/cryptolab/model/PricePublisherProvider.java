package com.budzek.cryptolab.model;

import io.reactivex.Flowable;
import scala.Enumeration;

@FunctionalInterface
public interface PricePublisherProvider{
    Flowable<Double> get(String fromCurrency, String toCurrency, Enumeration.Value  exchangeProvider);
}
